class Hero {
    constructor(name, img, level, health, strength, stamina, weapon) {
        this._name = name;
        this.img = img;
        this.level = level;
        this.health = health;
        this.strength = strength;
        this.stamina = stamina;
        this.weapon = weapon;
    }
    get name(){
        return this._name;
    }
}

class Mage extends Hero {
    constructor(name, img, level, health, strength, stamina, weapon, mana, spell) {
        super(name, img, level, health, strength, stamina, weapon);

        this.mana = mana;
        this.spell = spell;
    }
    realizeAttack(attacker, target) {
            target.health -= attacker.strength;
            alert(`${attacker.name} ha atacado ${target.name} con su ${attacker.weapon} y le ha hecho ${attacker.strength} puntos de daño`)
        }

}

class Dawrf extends Hero {
    constructor(name, img, level, health, strength, stamina, weapon) {
        super(name, img, level, health, strength, stamina, weapon);
    }
    realizeAttack(attacker, target) {
            target.health -= attacker.strength;
            alert(`${attacker.name} ha atacado ${target.name} con su ${attacker.weapon} y le ha hecho ${attacker.strength} puntos de daño`)
        }

}

class Archer extends Hero {
    constructor(name, img, level, health, strength, stamina, weapon, accuracy) {
        super(name, img, level, health, strength, stamina, weapon);

        this.accuracy = accuracy;
    }
    realizeAttack(attacker, target) {
            target.health -= attacker.strength;
            alert(`${attacker.name} ha atacado ${target.name} con su ${attacker.weapon} y le ha hecho ${attacker.strength} puntos de daño`)
        }

}


class Thief extends Hero {
    constructor(name, img, level, health, strength, stamina, weapon, stealth) {
        super(name, img, level, health, strength, stamina, weapon);

        this.stealth = stealth;
    }
    realizeAttack(attacker, target) {
            target.health -= attacker.strength;
            alert(`${attacker.name} ha atacado ${target.name} con su ${attacker.weapon} y le ha hecho ${attacker.strength} puntos de daño`)
        }

}

var merlin =new Mage("Merlín", "https://images-na.ssl-images-amazon.com/images/I/61BaDnR2OhL._SX425_.jpg", 1, 100, 7, 10, "staff", 16, "FireBall");
var gimly =new Dawrf("Gimly",  "https://www.virginmegastore.ae/medias/sys_master/root/h92/h58/9259321458718/740829-main.jpg", 1, 100, 17, 13, "axe");
var robin =new Archer("Robín", "https://media.artoyz.net/shop/20321-large_default/funko-pop-robin-hood.jpg", 1, 100, 9, 12, "bow", 18);
var lupin =new Thief("Lupin", "https://elenanofriki.com/6000-large_default/funko-pop-hagrid-con-pastel-15-cm-harry-potter.jpg", 1, 100, 12, 11, "daga", 20);


let arrayHeroes = [];
arrayHeroes.push(robin);
arrayHeroes.push(merlin);
arrayHeroes.push(gimly);
arrayHeroes.push(lupin);


/*for(i=0;i<arrayMuebles.length;i++) {
    var a = newDiv.cloneNode(true);
    a.id = "elemDiv" + i;

    a.querySelector("#nombre").innerHTML = arrayMuebles[i].nombre;
    a.querySelector("img").src = arrayMuebles[i].imagen;
    a.querySelector(".precio").innerHTML = arrayMuebles[i].precio + " €";

    var btn = a.querySelector("button");
    btn.id = i;
    btn.addEventListener("click", compra);
    document.querySelector("#colItems").appendChild(a);
}*/


var template = document.querySelector("#item");
var newDiv = template.content.querySelector("div");

for(i=0;i<arrayHeroes.length;i++) {
    var a = newDiv.cloneNode(true);
    a.id = "elemDiv" + i;

 
    
    var img = a.querySelector("#image");
    img.src = arrayHeroes[i].img;
    
    var title = a.querySelector("#nombre");
    title.innerHTML = arrayHeroes[i].name;

    var progress = a.querySelector("#progress");
    progress.value = arrayHeroes[i].health   ;

    var exp = a.querySelector(".exp");
    exp.innerHTML = arrayHeroes[i].level;

    var health = a.querySelector(".health");
    health.innerHTML = arrayHeroes[i].health;

    var spell = a.querySelector(".spell");
    spell.innerHTML = arrayHeroes[i].spell;

    var weapon = a.querySelector(".weapon");
    weapon.innerHTML = arrayHeroes[i].weapon;

    var stealth = a.querySelector(".stealth");
    
    stealth.innerHTML = arrayHeroes[i].stealth;
        if (!arrayHeroes[i].stealth) {
            stealth.innerHTML = 0;
        } 

    var btn = a.querySelector("button");
    btn.addEventListener("click", attack);

    var selector = a.querySelector("#select");
    arrayHeroes.forEach(function(heroe) {
        var opt= document.createElement("option");
        if (arrayHeroes[i].name != heroe.name) {
            opt.value=heroe.name;
            opt.text=heroe.name;
            (selector).add(opt);
        } 

      });    

    document.getElementById("colItems").appendChild(a);
}

function attack() {
    const attacker = arrayHeroes.find((heroe) => {
        return heroe.name === this.parentNode.querySelector("#nombre").innerHTML
    });

    const target = arrayHeroes.find((heroe) => {
        return heroe.name === this.parentNode.querySelector("#select").value
    });
    

    attacker.realizeAttack(attacker, target);


    
    let targetDiv = "#elemDiv" + (arrayHeroes.indexOf(target));
    var targetHealth = document.querySelector(targetDiv).querySelector(".health");
    targetHealth.innerHTML = target.health;
    //health.innerHTML = arrayHeroes[i].health;

    var targetProgress = document.querySelector(targetDiv).querySelector("#progress");
    targetProgress.value = target.health;
    if (targetProgress.value < 1) {
        document.querySelector(targetDiv).style.display= "none";
        console.log(document.querySelector(targetDiv));
    }

}


